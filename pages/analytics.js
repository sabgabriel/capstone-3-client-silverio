import React, { useState, useEffect, useContext } from 'react';
import {Fragment} from 'react';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap'
import UserContext from '../UserContext';
import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import Head from 'next/head'
import IncomeChart from '../components/IncomeChart';
import ExpensesChart from '../components/ExpensesChart';
import PieChart from '../components/PieChart';


export default function analytics(){
	let token = "";
	const { user, setUser } = useContext(UserContext)

	const [incomeData, setIncomeData] = useState([])
	const [expenseData, setExpenseData] = useState([])
	const [balance, setBalance] = useState([])
	const [category, setCategory] = useState([])

	useEffect(() => {
		token = localStorage.getItem('token')

		function handleBalance(){
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-latest-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const balance = data.map(data =>{
					return data
				})
	
				setBalance(balance)
			})
		}

		function getIncomeData(){
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-latest-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const income = data.filter(function (data){
					return data.type === "Income"
				})
		
				setIncomeData(income)
			})
		}
	
		
	
		function getExpenseData(){
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-latest-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const expense = data.filter(function (data){
					return data.type === "Expense"
				})
		
				setExpenseData(expense)
			})
		}

		function handleCategory(){
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-latest-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const categoryName = data.map(data =>{
					return data
				})
	
				setCategory(categoryName)
			})
		}

		//run funcs
		handleBalance()
		getIncomeData()
		getExpenseData()
		handleCategory()
	},[user])
	



	
    return (
		<Fragment>
			<Head>
                <title>Analytics</title>
            </Head>
			<div className="container">
				<br/>
				<br/>
				<IncomeChart rawData={incomeData} className="my-5"/>
				<hr className="my-5"/>
				<ExpensesChart rawData={expenseData} className="my-5"/>
				<hr className="my-5"/>
				<PieChart rawData={category} className="my-5"/>
				<hr className="my-5"/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</Fragment>
    )
}

