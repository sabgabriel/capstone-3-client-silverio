import React, {Fragment} from 'react';
import Banner from '../components/Banner';

export default function Error() {
	let data = {
		title: "Error 404",
		description: "The page you're looking for doesn't exist.",
		destination: "/",
		label: "Back to the Home Page"
	}

	return (

	<Fragment>
		<Banner dataProp={data}/>
	</Fragment>

	)
}