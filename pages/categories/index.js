import { useState, useEffect, useContext } from 'react';
import { Table, Jumbotron, Alert, Row, Col, Card, Button } from 'react-bootstrap'
import { Fragment} from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import Head from 'next/head'

export default function index(){
    const [categories, setCategories] = useState([])


    useEffect(() => {
        let token = localStorage.getItem('token')

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-categories`, {
            headers: {
                Authorization: `Bearer ${token}`
            } 
        })
        .then(res => res.json())
        .then(data => {

            // console.log(data)

            if(data._id !== null){

                setCategories(data)

            }else{

                setCategories([])

            }

        })

    },[])
	
    return (
        <Fragment>
            <Head>
                <title>Categories</title>
            </Head>
           <div className="records-display container">
	           <Row className="justify-content-md-end my-2">
	                <Link href="/categories/add">
	                    <Button variant="success" className="login-form-button">
	                        <a role="button">Add a Category</a>
	                    </Button>
	                </Link>
	            </Row>
	                {
		                categories.length > 0
		                ? 
		                <Table bordered hover>
		                    <thead>
		                        <tr className="login-text">
		                            <th>Category Name</th>
		                            <th>Category Type</th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                        {
		                        categories.map(category => {

		                            return( 
		                            
		                                <tr key={category._id}>  
		                                    <td>{category.name}</td>
		                                    <td>{category.type}</td>                                     
		                                </tr>

		                            )
	                        	})
	                        }
	                    </tbody>
	                </Table>
	                :   <Alert variant="info">No available categories yet</Alert>
	                }
            </div>
        </Fragment>
    )
}