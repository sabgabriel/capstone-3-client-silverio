import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import UserContext from '../../UserContext';
import Head from 'next/head'

export default function create() {

    //form input states
    let token = ""
    
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');

    const { user, setUser } = useContext(UserContext)


    useEffect(() => {
        token = localStorage.getItem('token')
    },[categoryName, categoryType])
    


    //function for adding new category
    function createCategory(e) {
        e.preventDefault();

        console.log(`${categoryName} is a category with a type: ${categoryType}.`);

        setCategoryName('');
        setCategoryType('');

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/add-category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: categoryName,
                typeName: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/categories')
        })
    }

    

    return (
        <Container>
            <Head>
                <title>Add Category</title>
            </Head>
            <div className="new-form container">
                <Form onSubmit={(e) => createCategory(e)}>
                    <Form.Group controlId="categoryType">
                        <Form.Label className="login-text font-weight-bold">Category Type:</Form.Label>
                        <Form.Control as="select" onChange={e => setCategoryType(e.target.value)} required>
                        	<option value="" disabled selected>Category Type</option>
                            <option value="Income">Income</option>
                            <option value="Expense">Expense</option>
                        </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="categoryName">
                        <Form.Label className="login-text font-weight-bold">Category Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
                    </Form.Group>

                    <Button variant="success" className="login-form-button" type="submit">Add Category</Button>
                </Form>
            </div>
        </Container>
    )
}
