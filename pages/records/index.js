import React, { useState, useEffect, useContext } from 'react';
import {Fragment} from 'react';
import UserContext from '../../UserContext';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap';
import Router from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import moment from 'moment';
import IncomeChart from '../../components/IncomeChart';
import ExpenseChart from '../../components/ExpensesChart';


export default function record({ usersData }){

	let token = ""
	
	const [records, setRecords] = useState([])
	const [searchBar, setSearchBar] = useState("")
	const [typeFilter, setTypeFilter] = useState("")
	const [recordsList, setRecordsList] = useState("")

	const { user, setUser } = useContext(UserContext)


	//search record by description, will run everytime value on search and filter changes
	useEffect(() => {
		token = localStorage.getItem('token')

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/search-record`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
				userId: user.userId,
				searchKeyword: searchBar,
				searchType: typeFilter
            })
        })
        .then(res => res.json())
        .then(data => {
			console.log(data)
			setRecordsList(data.map(data => {
				return (
					<div className="records-card my-2">
						<ListGroup.Item key={data._id}>
							<div className="record-title">
								<b>{data.description}</b>
							</div>
							{data.type}({data.categoryName})
							<br/>

							{
								data.type === "Income"
							? 
							<Fragment>	
								<div className="text-success record-card">
									+ ₱{data.amount}
									<br/>
								</div>
								<div className="text-secondary record-card">
									<b>Balance: ₱{data.balanceAfterTransaction}</b>
								</div>
							</Fragment>
							
							:	
							<Fragment>
								<div className="text-danger record-card">
									- ₱{data.amount}
									<br/>
								</div>
								<div className="text-secondary record-card">
									<b>Balance: ₱{data.balanceAfterTransaction}</b>
								</div>
							</Fragment>
			
							}
							
							<br/>
							{moment(data.dateAdded).format("YYYY-MM-DD")}
						</ListGroup.Item>
					</div>
				)
			}))
        })
	}, [searchBar, typeFilter])




	useEffect(() => {
		token = localStorage.getItem('token')

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-latest-records`, {
			method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				userId: user.userId
            })
		})
		.then(res => res.json())
		.then(data => {
			setRecordsList(data.map(data => {

				// console.log(data)
				//all
				return (
					
					<div className="records-card my-2">
						<ListGroup.Item key={data._id}>
							<div className="record-title">
								<b>{data.description}</b>
							</div>
							{data.type}({data.categoryName})
							<br/>
							{data.type === "Income"
							? 
							<Fragment>	
								<div className="text-success record-card">
									+ ₱{data.amount}
									<br/>
								</div>
								<div className="text-secondary record-card">
									<b>Balance: ₱{data.balanceAfterTransaction}</b>
								</div>
							</Fragment>
							
							:	
							<Fragment>
								<div className="text-danger record-card">
									- ₱{data.amount}
									<br/>
								</div>
								<div className="text-secondary record-card">
									<b>Balance: ₱{data.balanceAfterTransaction}</b>
								</div>
							</Fragment>
			
							}
							
							<br/>
							{moment(data.dateAdded).format("YYYY-MM-DD")}
						</ListGroup.Item>
					</div>
				)
			}))
		})

		
	}, [])
	
	
	
    return (
		<Fragment>
			<Head>
                <title>Records</title>
            </Head>
			<div className="records-display container">
				<Link href="/records/add">
					<Button variant="success" className="login-form-button">
						<a role="button">Add a Record</a>
					</Button>
				</Link>
				
				<Form.Group controlId="recordDesc">
						<Form.Control type="text" placeholder="Search Records (by description)" value={searchBar} onChange={e => setSearchBar(e.target.value)} required/>
					</Form.Group>
				<ListGroup>
				<Form.Group controlId="categoryType">
					<Form.Control as="select" type="text" defaultValue="All" onChange={e => setTypeFilter(e.target.value)} required>
						<option value="All">All</option>
						<option value="Income">Income</option>
						<option value="Expense">Expense</option>
					</Form.Control>
				</Form.Group>
					<div className="">
						{recordsList}
					</div>
				</ListGroup>
			</div>
		</Fragment>
    )
}

