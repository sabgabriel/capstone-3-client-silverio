import React, { useState, useEffect } from 'react';
import {Fragment} from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import Head from 'next/head'

export default function create() {
    let token = ""

    const [categories, setCategories] = useState([])
    const [newCat, setNewCat] = useState([])
    

    //declare form input states

    
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [recordAmount, setRecordAmount] = useState(0);
    const [recordDesc, setRecordDesc] = useState('');
    const [balBefore, setBalBefore] = useState(0);
    const [balAfter, setBalAfter] = useState(0);
    // const [dateAdded, setDateAdded] = useState('');

    useEffect(() => {
        token = localStorage.getItem('token')
    }, [categoryName, categoryType, recordAmount, recordDesc, balAfter])
    //function for processing creation of a new course
    function createRecord(e) {
        e.preventDefault();

        if(categoryType === "Income"){
            setBalAfter(parseInt(balBefore) + parseInt(recordAmount))
            
        }else if(categoryType === "Expense"){
            setBalAfter(parseInt(balBefore) - parseInt(recordAmount))
            
        }

        setBalBefore(parseInt(balAfter))

        //console.log(`${categoryName} is a category with a type: ${categoryType}.`);

        setCategoryName('');
        setCategoryType('');

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/add-record`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                typeName: categoryType,
                amount: recordAmount,
                description: recordDesc,
                balanceAfterTransaction: balAfter
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/records')
        })
    }


    useEffect(() => {
        const fetchData = async () => {
            try{
                const result = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/get-categories`, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    setCategories(data)             
                })
            } catch (err) {
                // error handling code
            }
        }

        fetchData()
    }, [])
    
    useEffect(() => {
    
        const newCat = categories.filter(function (category){
            return category.type === categoryType
        })

        setNewCat(newCat)
        
    }, [categoryType])

    console.log(newCat)

    
    

    return (
        <Fragment>
            <Head>
                <title>Add Record</title>
            </Head>
            <Container>
                <div className="new-form2">
                    <Form onSubmit={(e) => createRecord(e)}>
                        <Form.Group controlId="categoryType">
                            <Form.Label>Category Type:</Form.Label>
                            <Form.Control as="select" onChange={e => setCategoryType(e.target.value)} required>
                                <option value="" disabled selected>Select Category Type</option>
                                <option value="Income">Income</option>
                                <option value="Expense">Expense</option>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="categoryType">
                            <Form.Label>Category Name:</Form.Label>
                            <Form.Control as="select" onChange={e => setCategoryName(e.target.value)} required>
                            <option value="" disabled selected>Select Category Name</option>  
                                {
                                (categoryType !== "")
                                ?   
                                newCat.map(cat => {
                                        return(
                                            <option>{cat.name}</option>
                                        )
                                    })
                                : null
                                }
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="num1">
                            <Form.Label>Amount:</Form.Label>
                            <Form.Control type="number" value={recordAmount} onChange={e => setRecordAmount(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="recordDesc">
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" placeholder="Enter record description" value={recordDesc} onChange={e => setRecordDesc(e.target.value)} required/>
                        </Form.Group>

                        <Button variant="success" className="login-form-button" type="submit">Add Record</Button>
                    </Form>
                </div>
            </Container>
        </Fragment>
    )
}
