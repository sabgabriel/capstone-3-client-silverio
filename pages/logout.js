import React, {useContext, useEffect} from 'react';
import {Fragment} from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap'
import UserContext from '../UserContext';
import Router from 'next/router';
import Head from 'next/head';
import View from '../components/View'

export default function Logout() {

    const { unsetUser } = useContext(UserContext);
    

    function logoutUser() {

	        unsetUser();
	        Router.push('/login');
	}

    return (
		<Fragment>
			<Head>
                <title>Logout</title>
            </Head>
			<View title={ 'Logout' }>
				<Row className="justify-content-center logout-button">
					<Col xs md="6">
						<h3 className="text-center mb-4 my-5">Are you sure you wish to logout?</h3>
						<Button variant="success" type="submit" className="login-form-button" onClick={ logoutUser } block>Logout</Button>
					</Col>
				</Row>
			</View>
		</Fragment>
		
	)

}
