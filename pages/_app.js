import { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';
import { UserProvider } from '../UserContext';
import Footer from '../components/Footer';


function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({email: null})

	useEffect(() => {

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
          
          console.log(data.userId)

          	//if JWT is valid
			if(data.email){

				setUser({ email: data.email, userId: data.userId })

			//if JWT doesn't exist || invalid
			} else {
				setUser({

					email: null

				})
			}	
		})
		
        
        
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear();

		setUser({

			email: null

		})
	}

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
	  	<NavBar />
	  		<Component {...pageProps} />
	  	<Footer />
  	</UserProvider>
  )
}

export default MyApp
