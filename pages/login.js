import React, { useState, useEffect, useContext } from 'react';
import {Fragment} from 'react';
import { Card, Row, Col, Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Head from 'next/head';
import View from '../components/View';

import { GoogleLogin } from 'react-google-login';

export default function index() {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center m-0 p-0 login-container">
                <Col xs md="6" className="login-form">
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}

const LoginForm = () => {
    const { user, setUser} = useContext(UserContext);


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {

        e.preventDefault();


        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/login`, {
        	method: 'POST',
            headers: { 
            	'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password:password

        	})
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
            //successful authentication will return a JWT via the response accessToken property
            if(typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id})
                Router.push('/')

            } else {

                if (data.error === "does-not-exist") {

                    Swal.fire({

                        icon: "error",
                        title: "Login failed.",
                        text: "User does not exist."

                    })

                } else if (data.error === "incorrect-password"){
                    
                    Swal.fire ({

                        icon: "error",
                        title: "Login failed.",
                        text: "Email and password doesn't match."

                    })              	
                }
            }
        })
    }

    
    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [ email, password ])


        function authenticateGoogleToken(response){

        //response - google's response with our tokenId to be used to authenticate our google login user
        //response comes from google along with our user's details and token for authentication.
        console.log(response)

        //send our google login user's token id to the API server for 
        //authentication and retrieval of our own App's token to be used for logging into our App.

        //at this point, this URL endpoint: /verify-google-id-token hasn't been created yet.
        //Refer to part 2 of this Google Login step by step our server side.

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/verify-google-id-token`,{

            method: 'POST',
            headers: {

                'Content-Type': 'application/json'

            },
            body: JSON.stringify({

                tokenId: response.tokenId

            })

        })
        .then(res => res.json())
        .then(data => {
            //Refer to part 2 of this Google Login step by step our server side.

            //After receiving the return from our API server:

            //we will show alerts to show if the user logged in properly or if there are errors.
            if(typeof data.accessToken !== 'undefined'){

                //set the accessToken into our localStorage as token:
                localStorage.setItem('token', data.accessToken)

                //run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
                fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`,{

                    headers: {

                            'Authorization': `Bearer ${data.accessToken}`
                    }

                })
                .then(res => res.json())
                .then(data => {

                    localStorage.setItem('email', data.email)
                    localStorage.setItem('isAdmin', data.isAdmin)

                    //after getting the user's details, update the global user state:
                    setUser({

                        email: data.email,
                        isAdmin: data.isAdmin

                    })

                    //Fire a sweetalert to inform the user of successful login:
                    Swal.fire({

                        icon: 'success',
                        title: 'Successfully Logged-in'

                    })

                    Router.push('/records')
                })

            } else {

                //if data.accessToken is undefined, therefore, data contains a property called error instead.

                //This error will be shown if somehow our user's google token has an error or is compromised.
                if(data.error === "google-auth-error"){

                    Swal.fire({

                        icon: 'error',
                        title: 'Google Authentication Failed'


                    })

                } else if(data.error === "login-type-error"){

                    //This error will be shown if our user has already created an account in our app using the register page but is trying to use google login to log into our app:

                    Swal.fire({

                        icon: 'error',
                        title: 'Login Failed.',
                        text: 'You may have registered through a different procedure.'

                    })

                }

            }


        })

    }

 return(
        <Card>
            <Card.Header>Login</Card.Header>
            <Card.Body>
                        <Head>
                <title>Budget Tracker</title>
            </Head>
                <Form onSubmit={e => authenticate(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label className="login-text">Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label className="login-text">Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    </Form.Group>
                    <Row className ="justify-content-center">
                    { 
                    	isActive
                        ? 
                        <Button variant="success" className="btn-block login-form-button" type="submit">
                            Submit
                        </Button>
                        :
                        <Button variant="success" className="btn-block login-form-button" type="submit" disabled>
                            Submit
                        </Button>
                    }
                    </Row>
                 </Form>

                <GoogleLogin
                clientId="89145485364-f995uah1o595bmvaa8t57ljb1qv2693h.apps.googleusercontent.com"
                buttonText="Login using Google"
                onSuccess={ authenticateGoogleToken }
                onFailure={ authenticateGoogleToken }
                cookiePolicy={ 'single_host_origin' }
                className="d-flex justify-content-center btn-block"
            />
            </Card.Body>
        </Card>
    )
}