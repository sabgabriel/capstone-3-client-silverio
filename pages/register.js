import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import {Fragment} from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Swal from 'sweetalert2';

export default function register() {

    //states for form inputs
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')

    //state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)


    useEffect(() => {
        //validation to enable submit button when all fields are populated and both passwords match
        if(firstName!='' && lastName!='' && email != '' && (password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }

    }, [firstName, lastName, password1, password2])


    //function to register user
    function registerUser(e) {
        e.preventDefault();

        //check for duplicate email in database first
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {

            //if no duplicates found
            if (data === false){
                
                fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName:firstName,
                        lastName:lastName,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    //registration successful
                    if(data === true){

                        Swal.fire({
                            icon: "success",
                            title: "Successfully Registered.",
                            text: "Thank you for registering."

                        })

                        //redirect to login
                        Router.push('/login')

                    } else{

                        //redirect to error page
                        Router.push('/error')

                    }
                })
            }else{
            //duplicate email found
                Swal.fire({
                    icon: "error",
                    title: "Failed to register.",
                    text: "A user is already registered with this email."
                })

            }
        })
    } 

    return (

        <Fragment>
        	<div className="container register-container">
            <div className="register-form">
	            <Head>
	                <title>Register</title>
	            </Head>
	            <Form onSubmit={(e) => registerUser(e)}>
	                <Form.Group controlId="firstName">
	                    <Form.Label>First Name</Form.Label>
	                    <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
	                </Form.Group>
	                <Form.Group controlId="lastName">
	                    <Form.Label>Last Name</Form.Label>
	                    <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required />
	                </Form.Group>
	                <Form.Group controlId="userEmail">
	                    <Form.Label>Email address</Form.Label>
	                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
	                </Form.Group>

	                <Form.Group controlId="password1">
	                    <Form.Label>Password</Form.Label>
	                    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
	                </Form.Group>
	                <Form.Group controlId="password2">
	                    <Form.Label>Verify Password</Form.Label>
	                    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
	                </Form.Group>

					<Form.Text className="text-muted">
	                    We'll never share your details with anyone else.
	                </Form.Text>

	                {
	                    isActive
	 						? 
	 						<Button type="submit" id="submitBtn" className="register-form-button">Submit</Button>
	                        : 
	                        <Button type="submit" id="submitBtn" className="register-form-button" disabled>Submit</Button>
	                }
	                
	            </Form>
	        </div>
	        </div>
        </Fragment>
    )
}