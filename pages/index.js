import { useState, useContext } from 'react';
import {Fragment} from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import View from '../components/View'
import Head from 'next/head'
import Link from 'next/link'

export default function index() {

    return (
        <Fragment>
            <Head>
                <title>money eTrack</title>
            </Head>

            <section id="landing">
                <div class="landing-container-main">
                    <h1 class="landingHeading text-lg-center my-5">
                        TRACK<br/>your<br/>Spendings 
                    </h1>
                    <Link href="/register">
          						<div class="wrapper">
          						  <div class="link_wrapper">
          						    <a id="btnMain">Get Started</a>
          						    <div class="icon">
          						    </div>
          						  </div>
          						</div>             
                    </Link>
                      <p className="container text-center landing-desc">Money eTrack is a budget tracker app that helps you track your spendings.</p>
                </div>
            </section>

           {/*Landing 3*/}
            <section id="landing">
                <div className="landing-container">
                  <div className="row mx-auto container landing-container2">
                      <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                          <div>
                              <img className="img-fluid landing-icon1" src="./category.svg"/>
                      
                          </div>
                          <div className="text-center icon-text">
                              <h3 className="text-center">Categories</h3>
                              <p>
                                  This section lets you create a new category under Income or Expense for records creation.
                              </p>
                          </div>
                      </div>
                      <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                          <div>
                              <img className="img-fluid landing-icon1" src="./records.svg"/>
                             
                          </div>
                          <div className="text-center icon-text">
                              <h3 className="text-center">Records</h3>
                              <p>
                                  This section lets you create and see records within your own added categories. It lets you easily customize your budget records.
                              </p>
                          </div>
                      </div>
                      <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                          <div>
                              <img className="img-fluid landing-icon1" src="./analytics.svg"/>
                          </div>
                          <div className="text-center icon-text">
                              <h3 className="text-center">Analytics</h3>
                              <p>
                                  Analytical tools are used together with clean data visualization tools such as charts and graphs in order to help you find your patterns in your budget allocations.
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
            </section>
        </Fragment>
    )
}
