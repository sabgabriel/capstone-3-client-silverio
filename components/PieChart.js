import { useState, useEffect } from 'react';
import {Fragment} from 'react';
import {Pie} from 'react-chartjs-2';
import { colorRandomizer } from '../helpers/colorRandomizer';

export default function PieChart({ rawData }) {
    const [category, setCategory] = useState([])
    const [amount, setAmount] = useState([])
    const [bgColors, setBgColors] = useState([])

    useEffect(() => {
        console.log(rawData)
        if(rawData.length > 0){
           
            setCategory(rawData.map(element => element.categoryName))
            setAmount(rawData.map(element => element.amount))
            
            setBgColors(rawData.map(() => `#${colorRandomizer()}`))
        }
    }, [rawData])

 
    const data = {
        labels: category,
        datasets: [{
            data: amount,
            backgroundColor: bgColors,
            hoverBackgroundColor: 'rgba(231,15,110,1.0)'
        }]
    }

  
    return (
        <Fragment>
            <h3 className="text-center register-text">Category Breakdown</h3>
            <Pie data={data} />
        </Fragment>
    )
}
