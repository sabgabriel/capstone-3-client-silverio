import React from 'react';
import { Container } from 'react-bootstrap';
import Head from 'next/head';

const View = ({ title, children }) => {
    return (
        <React.Fragment>
            <Head>
                <title key="title-tag">{ title }</title>
                <meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <Container className="m-0 p-0" fluid>
                { children }
            </Container>
        </React.Fragment>
    )
}
export default View