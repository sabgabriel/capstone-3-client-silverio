import { useState, useEffect } from 'react';
import {Fragment} from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function BarChart({rawData}){
    const [months, setMonths] = useState([])
    const [monthlyIncome, setMonthlyIncome] = useState([])


    //Run a useEffect() to get all the available months OR the months that only has an income.
    useEffect(() => {
        if(rawData.length > 0){

            //placeholder array
            let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]

            
            //iterate over each object in the rawData array
            rawData.forEach(element => {
                if(!tempMonths.find(month => month === moment(element.dateAdded).format("MMMM"))){
                    tempMonths.push(moment(element.dateAdded).format("MMMM"))
                }    
            })

            const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

            tempMonths.sort((a, b) => {
                if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
                    return monthsRef.indexOf(a) - monthsRef.indexOf(b)
                }
            })

            setMonths(tempMonths)
        }
    }, [rawData])



    useEffect(() => {

        //computes the total sales of a particular month
        setMonthlyIncome(months.map(month => {
            let income = 0;
            rawData.forEach(element => {

                if(moment(element.dateAdded).format('MMMM') === month){
                    income = income + parseInt(element.amount)
                }
            })
            return income;

        }))

    }, [months])

    const data = {
        labels: months, //x-axis labels (bottom)
        datasets: [{
                label: 'Monthly Income for the Year 2021',
                backgroundColor: 'green',
                borderColor: 'white',
                borderWidth: 1,
                hoverBackgroundColor: 'lightblue',
                hoverBorderColor: 'black',
                data: monthlyIncome //determinant for bars
            }]
    }

        const options = {

            scales: {

                yAxes: [
                    {
                        ticks: {

                            beginAtZero: true,
                        //  max: 5000
                        }

                    }

                ]
            }

        }

    return(
        <Fragment>
            <h3 className="text-center register-text">Monthly Income</h3>
            <Bar data={data} options={options}/>
        </Fragment>
    )
}