import { useContext } from 'react';
import {Fragment} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../UserContext'

export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext)

    return (
		<Navbar bg="secondary" expand="lg">
			<Link href="/">
				<a className="navbar-brand">money E-Track</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav-right" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{
						user.email !== null
						?
	                        <Fragment>
								<Link href="/categories">
									<a className="nav-link" role="button">Categories</a>
								</Link>
								<Link href="/records">
									<a className="nav-link" role="button">Records</a>
								</Link>
	                            <Link href="/analytics">
                                    <a className="nav-link" role="button">Analytics</a>
                                </Link>
	                            <Link href="/budgetTrend">
                                    <a className="nav-link" role="button">Trend</a>
                                </Link>                            
								<Link href="/about">
									<a className="nav-link" role="button">About</a>
								</Link>
								<Link href="/logout">
									<a className="nav-link" role="button">Logout</a>
								</Link>
	                        </Fragment>
	                    :
	                    	<Fragment>
								<Link href="/login">
									<a className="nav-link" role="button">Login</a>
								</Link>
								<Link href="/register">
									<a className="nav-link" role="button">Register</a>
								</Link>
	                        </Fragment>
	                }
				</Nav>
			</Navbar.Collapse>
		</Navbar>
    )
}
