import 'react-bootstrap';
import Link from 'next/link'

export default function Footer() {

    return (
        <footer className="mr-auto fixed-bottom">
            <div>
                <div className="footer-container container-fluid m-0 p-0">	
                    <div className="bg-light justify-content-between">
                        <div className="footer py-3">

                           <p class="text-muted text-center"> money E-track | Rizhabeth Silverio &copy; 2021 </p>
                                                
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

