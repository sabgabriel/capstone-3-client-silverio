import { useState, useEffect } from 'react';
import {Fragment} from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function BarChart({rawData}){
    const [months, setMonths] = useState([])
    const [monthlyExpense, setMonthlyIncome] = useState([])

    useEffect(() => {
        if(rawData.length > 0){

            let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]
            
            rawData.forEach(element => {
                if(!tempMonths.find(month => month === moment(element.dateAdded).format("MMMM"))){
                    tempMonths.push(moment(element.dateAdded).format("MMMM"))
                }    
            })

            const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

            tempMonths.sort((a, b) => {
                if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
                    return monthsRef.indexOf(a) - monthsRef.indexOf(b)
                }
            })

            setMonths(tempMonths)
        }
    }, [rawData])

    useEffect(() => {
        setMonthlyIncome(months.map(month => {
            let expense = 0
            rawData.forEach(element => {
                // console.log(moment(element.dateAdded).format('MMMM'))
                if(moment(element.dateAdded).format('MMMM') === month){
                    expense = expense + parseInt(element.amount)
                }
            })
            return expense
        }))
    }, [months])

    const data = {
        labels: months,
        datasets: [
            {
                label: 'Monthly Expenses for the Year 2021',
                backgroundColor: 'red',
                borderColor: 'white',
                borderWidth: 1,
                hoverBackgroundColor: 'lightred',
                hoverBorderColor: 'black',
                data: monthlyExpense
            }
        ]
    }

    const options = {

        scales:{

            yAxes:[

                {

                    ticks: {

                        beginAtZero: true
                    }

                }

            ]

        }

    }

    return(
        <Fragment>
            <h3 className="text-center register-text">Monthly Expenses</h3>
            <Bar data={data} options={options}/>
        </Fragment>
    )
}