import { useState, useEffect } from 'react';
import {Fragment} from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

export default function LineChart({rawData}){

    const [monthlyBalance, setMonthlyBalance] = useState([])
    const [range, setRange] = useState([])

    useEffect(() => {
        if(rawData.length > 0){

            const balance = rawData.map(data => {
                return data.balanceAfterTransaction
            })

            balance.reverse()

            setMonthlyBalance(balance)

        }
    }, [rawData])

    useEffect(() => {

        setRange([0, 5000, 15000, 25000, 50000, 75000, 1000000, 125000, 150000])
        
    }, [monthlyBalance])

    const data = {
        labels: monthlyBalance,
        datasets: [
            {
                label: 'Balance Trend',
                backgroundColor: 'lightblue',
                borderColor: 'gray',
                borderWidth: 1,
                hoverBackgroundColor: 'blue',
                hoverBorderColor: 'white',
                data: monthlyBalance
            }
        ]
    }

    return(
        <Fragment>
            <h3 className="text-center register-text">Balance Trend</h3>
            <Line data={data}/>
        </Fragment>   
    )
}