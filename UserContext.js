import React from 'react';

//create a Context Object
const UserContext = React.createContext();

//Provider - subscribes to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;